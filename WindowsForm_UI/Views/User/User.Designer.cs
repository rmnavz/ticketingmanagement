﻿namespace WindowsForm_UI.Views.User
{
    partial class User
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menu = new System.Windows.Forms.MenuStrip();
            this.navHome = new System.Windows.Forms.ToolStripMenuItem();
            this.navAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.navEquipment = new System.Windows.Forms.ToolStripMenuItem();
            this.navLogs = new System.Windows.Forms.ToolStripMenuItem();
            this.navUser = new System.Windows.Forms.ToolStripMenuItem();
            this.Content = new MetroFramework.Controls.MetroPanel();
            this.editInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.navHome,
            this.navAccount,
            this.navEquipment,
            this.navLogs,
            this.navUser});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(680, 24);
            this.menu.TabIndex = 0;
            this.menu.Text = "menuStrip1";
            // 
            // navHome
            // 
            this.navHome.Name = "navHome";
            this.navHome.Size = new System.Drawing.Size(52, 20);
            this.navHome.Text = "Home";
            // 
            // navAccount
            // 
            this.navAccount.Name = "navAccount";
            this.navAccount.Size = new System.Drawing.Size(138, 20);
            this.navAccount.Text = "Account Management";
            this.navAccount.Click += new System.EventHandler(this.navAccount_Click);
            // 
            // navEquipment
            // 
            this.navEquipment.Name = "navEquipment";
            this.navEquipment.Size = new System.Drawing.Size(151, 20);
            this.navEquipment.Text = "Equipment Management";
            // 
            // navLogs
            // 
            this.navLogs.Name = "navLogs";
            this.navLogs.ShowShortcutKeys = false;
            this.navLogs.Size = new System.Drawing.Size(44, 20);
            this.navLogs.Text = "Logs";
            // 
            // navUser
            // 
            this.navUser.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.navUser.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editInfoToolStripMenuItem,
            this.logOutToolStripMenuItem});
            this.navUser.Name = "navUser";
            this.navUser.Size = new System.Drawing.Size(42, 20);
            this.navUser.Text = "User";
            // 
            // Content
            // 
            this.Content.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Content.HorizontalScrollbarBarColor = true;
            this.Content.HorizontalScrollbarHighlightOnWheel = false;
            this.Content.HorizontalScrollbarSize = 10;
            this.Content.Location = new System.Drawing.Point(0, 24);
            this.Content.Name = "Content";
            this.Content.Size = new System.Drawing.Size(680, 316);
            this.Content.TabIndex = 1;
            this.Content.VerticalScrollbarBarColor = true;
            this.Content.VerticalScrollbarHighlightOnWheel = false;
            this.Content.VerticalScrollbarSize = 10;
            // 
            // editInfoToolStripMenuItem
            // 
            this.editInfoToolStripMenuItem.Name = "editInfoToolStripMenuItem";
            this.editInfoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.editInfoToolStripMenuItem.Text = "Edit Info";
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.logOutToolStripMenuItem.Text = "Log Out";
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Content);
            this.Controls.Add(this.menu);
            this.Name = "Admin";
            this.Size = new System.Drawing.Size(680, 340);
            this.Load += new System.EventHandler(this.Admin_Load);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem navHome;
        private System.Windows.Forms.ToolStripMenuItem navAccount;
        private System.Windows.Forms.ToolStripMenuItem navEquipment;
        private System.Windows.Forms.ToolStripMenuItem navLogs;
        private System.Windows.Forms.ToolStripMenuItem navUser;
        private MetroFramework.Controls.MetroPanel Content;
        private System.Windows.Forms.ToolStripMenuItem editInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
    }
}
