﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using WindowsForm_UI.Libraries;
using WindowsForm_UI.Models;
using WindowsForm_UI.ViewModels;

namespace WindowsForm_UI.Views.Equipments
{
    public partial class AddEquipment : MetroForm
    {
        public AddEquipment()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (txtIDCode.Text != null)
            {
                EquipmentModel model = new EquipmentModel();

                model.IDCode = txtIDCode.Text;
                model.Description = txtDescription.Text;
                model.EquipmentTypeID = int.Parse(comboModel.SelectedValue.ToString());
                model.DepartmentID = int.Parse(ComboDepartment.SelectedValue.ToString());
                model.DateCreated = DateTime.Now;
                model.IsEnabled = true;
                model.IsActive = true;

                EquipmentViewModel viewmodel = new EquipmentViewModel();
                viewmodel.Add(model);

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void AddAccount_Load(object sender, EventArgs e)
        {
            DepartmentViewModel Department = new DepartmentViewModel();

            ComboDepartment.DataSource = new BindingSource(Department.GetList(), null);
            ComboDepartment.DisplayMember = "Name";
            ComboDepartment.ValueMember = "ID";

            EquipmentTypeViewModel Equipment = new EquipmentTypeViewModel();

            comboModel.DataSource = new BindingSource(Equipment.GetList(), null);
            comboModel.DisplayMember = "Name";
            comboModel.ValueMember = "ID";
        }
    }
}
