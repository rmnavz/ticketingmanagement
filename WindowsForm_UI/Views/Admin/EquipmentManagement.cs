﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using WindowsForm_UI.Views.Accounts;
using WindowsForm_UI.ViewModels;
using WindowsForm_UI.Models;
using System.Threading;
using WindowsForm_UI.Views.Equipments;

namespace WindowsForm_UI.Views.Admin
{
    public partial class EquipmentManagement : MetroUserControl
    {
        public EquipmentManagement()
        {
            InitializeComponent();
        }
        EquipmentViewModel Equipment = new EquipmentViewModel();
        private Thread thread;
        private string Search;
        private void EquipmentManagement_Load(object sender, EventArgs e)
        {
            this.Width = this.Parent.Width;
            this.Height = this.Parent.Height;

            this.Parent.Resize += Parent_Resize;

            thread = new Thread(PopulateData);
            thread.Start();
        }

        private void PopulateData()
        {
            Gridtable.Invoke(new MethodInvoker(delegate { Gridtable.DataSource = ""; }));
            Gridtable.Invoke(new MethodInvoker(delegate { Gridtable.DataSource = new BindingSource(Equipment.GetList("Admin", Search), null); }));
        }

        private void Parent_Resize(object sender, EventArgs e)
        {
            this.Width = this.Parent.Width;
            this.Height = this.Parent.Height;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddEquipment Add = new AddEquipment();
            DialogResult dialogResult = Add.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                thread = new Thread(PopulateData);
                thread.Start();
            }
            else if (dialogResult == DialogResult.Cancel)
            {

            }
        }

        //private void btnEdit_Click(object sender, EventArgs e)
        //{
        //    if(Gridtable.SelectedRows.Count == 1)
        //    {
        //        AccountModel model = (AccountModel)Gridtable.CurrentRow.DataBoundItem;

        //        EditAccount Add = new EditAccount(model);
        //        DialogResult dialogResult = Add.ShowDialog();

        //        if (dialogResult == DialogResult.OK)
        //        {
        //            thread = new Thread(PopulateData);
        //            thread.Start();
        //        }
        //        else if (dialogResult == DialogResult.Cancel)
        //        {

        //        }

        //    }
        //}

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Gridtable.SelectedRows.Count == 1)
            {
                AccountModel model = (AccountModel)Gridtable.CurrentRow.DataBoundItem;

                AccountViewModel viewmodel = new AccountViewModel();

                viewmodel.Delete(model);

                thread = new Thread(PopulateData);
                thread.Start();

            }
        }

        private void SearchBar_TextChanged(object sender, EventArgs e)
        {
            thread = new Thread(PopulateData);
            thread.Start();
        }
    }
}
