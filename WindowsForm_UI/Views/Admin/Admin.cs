﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;
using WindowsForm_UI.Models;
using WindowsForm_UI.Libraries;
using WindowsForm_UI.ViewModels;

namespace WindowsForm_UI.Views.Admin
{
    public partial class Admin : MetroUserControl
    {
        public Admin()
        {
            InitializeComponent();
        }

        private void Admin_Load(object sender, EventArgs e)
        {
            this.Parent.Resize += Parent_Resize;
            this.Parent_Resize(this, EventArgs.Empty);

            var auth0 = (AccountModel)Sessions.Get("Auth0");
            navUser.Text = auth0.LastName + ", " + auth0.FirstName + " " + auth0.MiddleName;
        }

        private void Parent_Resize(object sender, EventArgs e)
        {
            this.Width = this.Parent.Width;
            this.Height = this.Parent.Height;

            if (this.Width <= 420)
            {
                menu.Dock = DockStyle.Left;
            }
            else
            {
                menu.Dock = DockStyle.Top;
            }
        }

        private void navAccount_Click(object sender, EventArgs e)
        {
            Content.Controls.Clear();
            Content.Controls.Add(new AccountManagement());
        }

        private void navEquipment_Click(object sender, EventArgs e)
        {
            Content.Controls.Clear();
            Content.Controls.Add(new EquipmentManagement());
        }

        private void navLogs_Click(object sender, EventArgs e)
        {
            Content.Controls.Clear();
            Content.Controls.Add(new LogManagement());
        }

        private void navHome_Click(object sender, EventArgs e)
        {
            Content.Controls.Clear();
            Content.Controls.Add(new Home());
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AccountViewModel Account = new AccountViewModel();
            Account.Logout();
            Application.Restart();
        }
    }
}
