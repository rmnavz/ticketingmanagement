﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using WindowsForm_UI.Libraries;
using WindowsForm_UI.Models;
using WindowsForm_UI.ViewModels;

namespace WindowsForm_UI.Views.Accounts
{
    public partial class EditAccount : MetroForm
    {
        public EditAccount(AccountModel amodel)
        {
            model = amodel;
            InitializeComponent();
        }

        AccountModel model = new AccountModel();
        private void EditAccount_Load(object sender, EventArgs e)
        {
            DepartmentViewModel Department = new DepartmentViewModel();

            ComboDepartment.DataSource = new BindingSource(Department.GetList(), null);
            ComboDepartment.DisplayMember = "Name";
            ComboDepartment.ValueMember = "ID";

            UserRoleViewModel UserRole = new UserRoleViewModel();

            comboUserRole.DataSource = new BindingSource(UserRole.GetList(), null);
            comboUserRole.DisplayMember = "Name";
            comboUserRole.ValueMember = "ID";

            txtEmail.Text = model.Email;
            txtFirstname.Text = model.FirstName;
            txtMiddlename.Text = model.MiddleName;
            txtLastname.Text = model.LastName;
            comboUserRole.SelectedValue = model.UserRoleID;
            ComboDepartment.SelectedValue = model.DepartmentID;
            txtPassword.Text = StringCipher.Decrypt(model.Password, AccountSecurity.EncryptionKey);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            model.Email = txtEmail.Text;
            model.FirstName = txtFirstname.Text;
            model.MiddleName = txtMiddlename.Text;
            model.LastName = txtLastname.Text;
            model.UserRoleID = int.Parse(comboUserRole.SelectedValue.ToString());
            model.DepartmentID = int.Parse(ComboDepartment.SelectedValue.ToString());
            model.Password = StringCipher.Encrypt(txtPassword.Text, txtEmail.Text);
            //model.IsEnabled = true;
            //model.IsVerified = true;

            AccountViewModel viewmodel = new AccountViewModel();
            viewmodel.Update(model);

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
