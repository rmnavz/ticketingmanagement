﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using WindowsForm_UI.Libraries;
using WindowsForm_UI.Models;
using WindowsForm_UI.ViewModels;

namespace WindowsForm_UI.Views.Accounts
{
    public partial class AddAccount : MetroForm
    {
        public AddAccount()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Regex r = new Regex("^[a-zA-Z0-9]*$");
            if (r.IsMatch(txtPassword.Text))
            {
                AccountModel model = new AccountModel();

                model.Email = txtEmail.Text;
                model.FirstName = txtFirstname.Text;
                model.MiddleName = txtMiddlename.Text;
                model.LastName = txtLastname.Text;
                model.UserRoleID = int.Parse(comboUserRole.SelectedValue.ToString());
                model.DepartmentID = int.Parse(ComboDepartment.SelectedValue.ToString());
                model.DateCreated = DateTime.Now;
                model.Password = StringCipher.Encrypt(txtPassword.Text, AccountSecurity.EncryptionKey);
                model.IsEnabled = true;
                model.IsVerified = true;

                AccountViewModel viewmodel = new AccountViewModel();
                viewmodel.Register(model);

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void AddAccount_Load(object sender, EventArgs e)
        {
            DepartmentViewModel Department = new DepartmentViewModel();

            ComboDepartment.DataSource = new BindingSource(Department.GetList(), null);
            ComboDepartment.DisplayMember = "Name";
            ComboDepartment.ValueMember = "ID";

            UserRoleViewModel UserRole = new UserRoleViewModel();

            comboUserRole.DataSource = new BindingSource(UserRole.GetList(), null);
            comboUserRole.DisplayMember = "Name";
            comboUserRole.ValueMember = "ID";
        }
    }
}
