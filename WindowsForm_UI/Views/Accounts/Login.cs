﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using WindowsForm_UI.Models;
using WindowsForm_UI.ViewModels;
using System.Text.RegularExpressions;
using System.Threading;

namespace WindowsForm_UI.Views.Accounts
{
    public partial class Login : MetroForm
    {
        public Login()
        {
            InitializeComponent();
        }
        private Thread thread;
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Regex r = new Regex("^[a-zA-Z0-9]*$");
            if (r.IsMatch(txtPassword.Text))
            {
                AccountModel model = new AccountModel();

                model.Email = txtEmail.Text;
                model.Password = txtPassword.Text;

                AccountViewModel accountViewModel = new AccountViewModel();

                if (accountViewModel.Login(model))
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    Cursor.Current = Cursors.Arrow;
                    MessageBox.Show("The username and password does not match!");
                }
            }
        }
    }
}
