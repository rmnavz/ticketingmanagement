﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Forms;
using WindowsForm_UI.Libraries;
using WindowsForm_UI.Models;
using WindowsForm_UI.Views.Accounts;
using WindowsForm_UI.Views.Admin;
using WindowsForm_UI.Views.User;

namespace WindowsForm_UI
{
    public partial class Main : MetroForm
    {
        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            Container.Width = this.Width;
            Container.Height = this.Height;

            var settings = ConfigurationManager.ConnectionStrings[0];
            var fi = typeof(ConfigurationElement).GetField(
                          "_bReadOnly",
                          BindingFlags.Instance | BindingFlags.NonPublic);
            fi.SetValue(settings, false);
            settings.ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=" + Path.Combine(Environment.CurrentDirectory, @"Data\") + "Database.mdf" + ";Integrated Security=True";

            Sessions.Start();
            //AccountSecurity.Authenticate(new AccountModel() { Email = "migznava16@gmail.com", Password = "12345678", UserRoleID = 1 });
            //Console.WriteLine(AccountSecurity.IsAuthenticated().ToString());
            if (!AccountSecurity.IsAuthenticated())
            {
                
                Login loginpage = new Login();
                DialogResult dialogResult = loginpage.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {
                    pageload();
                }
                else if (dialogResult == DialogResult.Cancel)
                {
                    Application.Exit();
                }
                else
                {
                    Application.Exit();
                }

            }
            else
            {
                pageload();
            }
        }

        private void pageload()
        {
            var auth0 = (AccountModel)Sessions.Get("Auth0");

            if (auth0.UserRoleID == 1)
            {
                this.Container.Controls.Add(new Admin());
            }
            else if(auth0.UserRoleID == 2)
            {
                this.Container.Controls.Add(new User());
            }
            else
            {

            }
        }

        private void Main_Resize(object sender, EventArgs e)
        {
            Container.Width = this.Width;
            Container.Height = this.Height;
        }
    }
}
