﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsForm_UI.Libraries
{
    public static class Sessions
    {
        private static Dictionary<string, object> SessionList { get; set; }

        public static void Start()
        {
            SessionList = new Dictionary<string, object>();
        }

        public static void Add(string Key, object Value)
        {
            if(SessionList.ContainsKey(Key))
            {
                SessionList[Key] = Value;
            }

            SessionList.Add(Key, Value);
        }

        public static object Get(string Key)
        {
            if(SessionList.ContainsKey(Key))
            {
                return SessionList[Key];
            }
            else
            {
                return new object();
            }
        }

        public static void Remove(string Key)
        {
            if (SessionList.ContainsKey(Key))
            {
                SessionList.Remove(Key);
            }
        }
    }
}
