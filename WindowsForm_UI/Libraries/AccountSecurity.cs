﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsForm_UI.Models;

namespace WindowsForm_UI.Libraries
{
    public class AccountSecurity
    {
        public static string EncryptionKey = "aumain";
        public static void Authenticate(AccountModel model)
        {
            Sessions.Add("Auth0", model);
            Sessions.Add("Token", StringCipher.Encrypt(model.Email, EncryptionKey));
        }

        public static void Destroy()
        {
            Sessions.Remove("Auth0");
            Sessions.Remove("Token");
        }

        public static bool IsAuthenticated()
        {
            try
            { 
                var auth0 = (AccountModel)Sessions.Get("Auth0");
                if (auth0.Email == StringCipher.Decrypt(Sessions.Get("Token").ToString(), EncryptionKey))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
