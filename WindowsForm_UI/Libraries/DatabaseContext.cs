﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsForm_UI.Models;

namespace WindowsForm_UI.Libraries
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("DefaultConnection") { }

        public DbSet<AccountModel> Accounts { get; set; }
        public DbSet<DepartmentModel> Departments { get; set; }
        public DbSet<EquipmentModel> Equipments { get; set; }
        public DbSet<EquipmentTypeModel> EquipmentTypes { get; set; }
        public DbSet<LogModel> Logs { get; set; }
        public DbSet<UserRoleModel> UserRoles { get; set; }
    }
}
