﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsForm_UI.Models
{
    public class AccountModel
    {
        public int ID { get; set; }
        public string IDCode { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int UserRoleID { get; set; }
        public int DepartmentID { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsVerified { get; set; }
    }
}
