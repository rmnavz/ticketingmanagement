﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsForm_UI.Models
{
    public class EquipmentTypeModel
    {
        public int ID { get; set; }
        public string IDCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
