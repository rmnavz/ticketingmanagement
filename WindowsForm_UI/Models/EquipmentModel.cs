﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsForm_UI.Models
{
    public class EquipmentModel
    {
        public int ID { get; set; }
        public string IDCode { get; set; }
        public string Prefix { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int DepartmentID { get; set; }
        public int EquipmentTypeID { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsActive { get; set; }
    }
}
