﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsForm_UI.Models
{
    public class LogModel
    {
        public int ID { get; set; }
        public string IDCode { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }
        public int ActionLevel { get; set; }
        public int AccountID { get; set; }
        public DateTime DateRecorded { get; set; }
    }
}
