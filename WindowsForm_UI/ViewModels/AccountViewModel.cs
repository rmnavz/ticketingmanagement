﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsForm_UI.Libraries;
using WindowsForm_UI.Models;

namespace WindowsForm_UI.ViewModels
{
    class AccountViewModel
    {
        DatabaseContext Db = new DatabaseContext();
        LogViewModel log = new LogViewModel();
        public List<AccountModel> GetList(string Access = "", string Search = "")
        {
            if(Access == "Admin")
            {
                return Db.Accounts.ToList();
            }
            else if(Access == "User")
            {
                AccountModel model = (AccountModel)Sessions.Get("Auth0");
                return Db.Accounts.Where(m => m.ID == model.ID).ToList();
            }
            else
            {
                AccountModel model = (AccountModel)Sessions.Get("Auth0");
                return new List<AccountModel>() { model };
            }
            
        }

        public bool Login(AccountModel model)
        {
            
            AccountModel result = Db.Accounts.Where(m => m.Email == model.Email).FirstOrDefault();

            if (result == null)
            {
                return false;
            }

            if (StringCipher.Decrypt(result.Password, AccountSecurity.EncryptionKey) == model.Password)
            {
                AccountSecurity.Authenticate(result);
                log.Add("Login", "Account Login", 3);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Register(AccountModel model)
        {
            
            var result = Db.Accounts.Where(m => m.Email == model.Email);

            if (result.Count() > 0)
            {
                return false;
            }
            else
            {
                try
                {
                    Db.Accounts.Add(model);
                    Db.SaveChangesAsync();
                    log.Add("Register", "Account Register", 3);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public bool Update(AccountModel model)
        {
            try
            {
                AccountModel result = Db.Accounts.Where(m => m.ID == model.ID).ToList<AccountModel>().FirstOrDefault();
                result = model;
                Db.SaveChangesAsync();
                log.Add("Update", "Account Update", 3);
                return true;
            }
            catch
            {
                return false;
            }

        }

        public bool Delete(AccountModel model)
        {
            try
            {
                AccountModel result = Db.Accounts.Where(m => m.ID == model.ID).ToList<AccountModel>().FirstOrDefault();
                //result = model;
                Db.Accounts.Remove(result);
                Db.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Logout()
        {
            AccountSecurity.Destroy();
            log.Add("Logout", "Account Logout", 3);
            return true;
        }
    }
}
