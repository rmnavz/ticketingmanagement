﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsForm_UI.Libraries;
using WindowsForm_UI.Models;

namespace WindowsForm_UI.ViewModels
{
    public class LogViewModel
    {
        DatabaseContext Db = new DatabaseContext();

        public List<LogModel> GetList(string Access = "", string Search = "")
        {
            return Db.Logs.ToList();

        }

        public bool Add(string Action, string Description, int ActionLevel)
        {
            try
            {
                AccountModel Account = (AccountModel)Sessions.Get("Auth0");
                LogModel model = new LogModel();

                model.Action = Action;
                model.Description = Description;
                model.ActionLevel = ActionLevel;
                model.AccountID = Account.ID;
                model.DateRecorded = DateTime.Now;

                Db.Logs.Add(model);
                Db.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
