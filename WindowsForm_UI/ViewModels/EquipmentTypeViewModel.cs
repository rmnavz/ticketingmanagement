﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsForm_UI.Libraries;
using WindowsForm_UI.Models;

namespace WindowsForm_UI.ViewModels
{
    class EquipmentTypeViewModel
    {
        DatabaseContext Db = new DatabaseContext();
        LogViewModel log = new LogViewModel();

        public List<EquipmentTypeModel> GetList(string Access = "", string Search = "")
        {
            return Db.EquipmentTypes.ToList();
        }
    }
}
