﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsForm_UI.Libraries;
using WindowsForm_UI.Models;

namespace WindowsForm_UI.ViewModels
{
    class EquipmentViewModel
    {
        DatabaseContext Db = new DatabaseContext();
        LogViewModel log = new LogViewModel();

        public List<EquipmentModel> GetList(string Access = "", string Search = "")
        {
            if (Access == "Admin")
            {
                return Db.Equipments.ToList();
            }
            else
            {
                return new List<EquipmentModel>();
            }

        }

        public bool Add(EquipmentModel model)
        {
            try
            {
                var result = Db.Equipments.Where(m => m.IDCode == model.IDCode);

                if (result.Count() > 0)
                {
                    return false;
                }
                else
                {
                    try
                    {
                        Db.Equipments.Add(model);
                        Db.SaveChangesAsync();
                        log.Add("Add Equipment", "Equipment Added", 3);
                        return true;
                    }
                    catch
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
