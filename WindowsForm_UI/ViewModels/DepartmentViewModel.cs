﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsForm_UI.Libraries;
using WindowsForm_UI.Models;

namespace WindowsForm_UI.ViewModels
{
    public class DepartmentViewModel
    {
        DatabaseContext Db = new DatabaseContext();

        public List<DepartmentModel> GetList()
        {
           return Db.Departments.ToList();
        }
    }
}
