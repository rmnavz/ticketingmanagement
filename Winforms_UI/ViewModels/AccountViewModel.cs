﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Winforms_UI.Libraries;
using Winforms_UI.Models;

namespace Winforms_UI.ViewModels
{
    public class AccountViewModel
    {
        DatabaseContext Db = new DatabaseContext();

        public bool Login(AccountModel model)
        {
            AccountModel result = Db.Accounts.Where(m => m.Username == model.Username).FirstOrDefault();

            if(StringCipher.Decrypt(result.Password, model.Username) == model.Password)
            {
                AccountSecurity.Login(result);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Register(AccountModel model)
        {
            var result = Db.Accounts.Where(m => m.Username == model.Username);

            if(result.Count() > 0)
            {
                return false;
            }
            else
            {
                try
                {
                    Db.Accounts.Add(model);
                    Db.SaveChangesAsync();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public bool update(AccountModel model)
        {
            try
            {
                AccountModel result = Db.Accounts.Where(m => m.ID == model.ID).ToList<AccountModel>().FirstOrDefault();
                result = model;
                Db.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }

        }

        
    }
}
