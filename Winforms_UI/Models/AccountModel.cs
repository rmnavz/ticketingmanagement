﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Winforms_UI.Libraries;

namespace Winforms_UI.Models
{
    [Table("Accounts")]
    public class AccountModel
    {
        public int ID { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int UserRoleID { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsVerified { get; set; }
    }
}
