﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Winforms_UI.Models
{
    [Table("Departments")]
    public class DepartmentModel
    {
        public int ID { get; set; }
        public string Prefix { get; set; }
        public string Name { get; set; }
        public string Definition { get; set; }
        public List<int> UserID { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsEnabled { get; set; }
    }
}
