﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Winforms_UI.Models
{
    [Table("Tickets")]
    public class TicketModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Definition { get; set; }
        public int StatusID { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateFinished { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsVerified { get; set; }
    }
}
