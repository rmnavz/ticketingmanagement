﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Winforms_UI.Models
{
    [Table("UserRoles")]
    public class UserRoleModel
    {
        public int ID { get; set; }
        public string Prefix { get; set; }
        public string Role { get; set; }
    }
}
