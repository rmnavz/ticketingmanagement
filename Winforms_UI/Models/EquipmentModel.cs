﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Winforms_UI.Models
{
    [Table("Equipments")]
    public class EquipmentModel
    {
        public int ID { get; set; }
        public string Prefix { get; set; }
        public string Name { get; set; }
        public string Definition { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsEnabled { get; set; }
    }
}
