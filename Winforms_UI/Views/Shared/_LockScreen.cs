﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Winforms_UI.Views.Shared
{
    public partial class _LockScreen : MetroFramework.Controls.MetroUserControl
    {
        public _LockScreen()
        {
            InitializeComponent();
        }

        private void _LockScreen_Load(object sender, EventArgs e)
        {
            this.Width = Parent.Width;
            this.Height = Parent.Height;
        }
    }
}
