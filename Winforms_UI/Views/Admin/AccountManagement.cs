﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;

namespace Winforms_UI.Views.Admin
{
    public partial class AccountManagement : MetroUserControl
    {
        public AccountManagement()
        {
            InitializeComponent();
        }

        private void AccountManagement_Load(object sender, EventArgs e)
        {
            this.Width = this.Parent.Parent.Width;
            this.Height = this.Parent.Parent.Height;

            int column = this.Width / 200;          

            ContainerPanel.Controls.Clear();

            for (int i = 0; i < 15; i += column)
            {
                MetroTile Tile = new MetroTile()
                {
                    Width = 150,
                    Height = 150,
                    Location = new Point(i ,0)
                };
                ContainerPanel.Controls.Add(Tile);
            }
        }
    }
}
