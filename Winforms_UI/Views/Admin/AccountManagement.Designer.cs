﻿namespace Winforms_UI.Views.Admin
{
    partial class AccountManagement
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ContainerPanel = new MetroFramework.Controls.MetroPanel();
            this.SuspendLayout();
            // 
            // ContainerPanel
            // 
            this.ContainerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContainerPanel.HorizontalScrollbarBarColor = true;
            this.ContainerPanel.HorizontalScrollbarHighlightOnWheel = false;
            this.ContainerPanel.HorizontalScrollbarSize = 10;
            this.ContainerPanel.Location = new System.Drawing.Point(20, 60);
            this.ContainerPanel.Name = "ContainerPanel";
            this.ContainerPanel.Size = new System.Drawing.Size(728, 340);
            this.ContainerPanel.TabIndex = 0;
            this.ContainerPanel.Theme = MetroFramework.MetroThemeStyle.Light;
            this.ContainerPanel.VerticalScrollbarBarColor = true;
            this.ContainerPanel.VerticalScrollbarHighlightOnWheel = false;
            this.ContainerPanel.VerticalScrollbarSize = 10;
            // 
            // AccountManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 420);
            this.Controls.Add(this.ContainerPanel);
            this.Name = "AccountManagement";
            this.Text = "Account";
            this.Load += new System.EventHandler(this.AccountManagement_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel ContainerPanel;
    }
}
