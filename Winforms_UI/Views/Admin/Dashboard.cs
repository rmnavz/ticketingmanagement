﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Controls;

namespace Winforms_UI.Views.Admin
{
    public partial class Dashboard : MetroUserControl
    {
        public Dashboard()
        {
            InitializeComponent();
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {
            this.Width = this.Parent.Width;
            this.Height = this.Parent.Height;

            TabPage tab1 = new TabPage("Accounts");
            TabPage tab2 = new TabPage("Equipments");
            TabPage tab3 = new TabPage("Tickets");

            tab1.Controls.Clear();
            tab1.Controls.Add(new AccountManagement());

            MetroTabs.TabPages.Add(tab1);
            MetroTabs.TabPages.Add(tab2);
            MetroTabs.TabPages.Add(tab3);
        }
    }
}
