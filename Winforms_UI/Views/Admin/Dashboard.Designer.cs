﻿namespace Winforms_UI.Views.Admin
{
    partial class Dashboard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MetroTabs = new MetroFramework.Controls.MetroTabControl();
            this.SuspendLayout();
            // 
            // MetroTabs
            // 
            this.MetroTabs.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.MetroTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MetroTabs.Location = new System.Drawing.Point(20, 60);
            this.MetroTabs.Name = "MetroTabs";
            this.MetroTabs.Size = new System.Drawing.Size(728, 340);
            this.MetroTabs.TabIndex = 0;
            this.MetroTabs.Theme = MetroFramework.MetroThemeStyle.Light;
            this.MetroTabs.UseSelectable = true;
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 420);
            this.Controls.Add(this.MetroTabs);
            this.Name = "Dashboard";
            this.Text = "Admin";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl MetroTabs;
    }
}
