﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;
using Winforms_UI.Models;
using Winforms_UI.ViewModels;

namespace Winforms_UI.Views.Account
{
    public partial class Login : MaterialForm
    {
        public Login()
        {
            InitializeComponent();

            // Create a material theme manager and add the form to manage (this)
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;

            // Configure color schema
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.Blue400, Primary.Blue500,
                Primary.Blue500, Accent.LightBlue200,
                TextShade.WHITE
            );

            this.AcceptButton = btnLogin;
        }

        protected bool inputusername = true;
        protected bool inputpassword = true;

        public List<string> ErrorMessage { get; set; }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void txtUsername_Enter(object sender, EventArgs e)
        {
            if(inputusername)
            {
                inputusername = false;
                txtUsername.Clear();
                txtUsername.ForeColor = Color.Black;
            }
            
        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {
            if(inputpassword)
            {
                inputpassword = false;
                txtPassword.Clear();
                txtPassword.PasswordChar = '*';
                txtPassword.ForeColor = Color.Black;
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            AccountModel model = new AccountModel();
            model.Username = txtUsername.Text;
            model.Password = txtPassword.Text;

            AccountViewModel accountViewModel = new AccountViewModel();

            if (accountViewModel.Login(model))
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                lblError.Text = "The username and password does not match!";
            }
        }
    }
}
