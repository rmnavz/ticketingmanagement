﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Winforms_UI.Libraries
{
    static class Session
    {
        private static Dictionary<string,object> SessionList { get; set; }

        public static void Start()
        {
            SessionList = new Dictionary<string, object>();
        }

        public static void Add(string Name, object value)
        {
            if (SessionList.ContainsKey(Name))
            {
                SessionList[Name] = value;
            }
            else
            {
                SessionList.Add(Name, value);
            }
        }

        public static void Remove(string Name)
        {
            SessionList.Remove(Name);
        }

        public static object Get(string Name)
        {
            if(SessionList.ContainsKey(Name) == true)
            {
                return SessionList[Name];
            }
            else
            {
                return null;
            }
        }
    }
}
