﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Winforms_UI.Models;

namespace Winforms_UI.Libraries
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("DefaultConnection") { }

        public DbSet<AccountModel> Accounts { get; set; }
        public DbSet<UserRoleModel> UserRole { get; set; }
        public DbSet<DepartmentModel> Departments { get; set; }
        public DbSet<TicketModel> Tickets { get; set; }
        public DbSet<EquipmentModel> Equipments { get; set; }
    }
}
