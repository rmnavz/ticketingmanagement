﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Winforms_UI.Libraries
{
    public class GridSystem
    {
        public void Breakpoints(Form form)
        {
            if(form.Width >= 1200)
            {
                Session.Add("Screen", "XL");
            }
            else if(form.Width >= 992)
            {
                Session.Add("Screen", "L");
            }
            else if(form.Width >= 768)
            {
                Session.Add("Screen", "M");
            }
            else if (form.Width >= 576)
            {
                Session.Add("Screen", "S");
            }
            else if (form.Width < 576)
            {
                Session.Add("Screen", "XS");
            }
            else
            {
                Session.Add("Screen", "M");
            }
        }
    }
}
