﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Winforms_UI.Models;

namespace Winforms_UI.Libraries
{
    class AccountSecurity
    {
        public static bool IsLoggedin()
        {
            if (Session.Get("uauth") != null)
            {
                return true;
            }
            return false;
        }

        public static void Login(AccountModel model)
        {
            
            Session.Add("uauth", "");
            Session.Add("ulog", model);
            
        }
    }
}
