﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Winforms_UI.Libraries
{
    class InputValidator
    {

        public static bool Username(string value, int level = 0)
        {
            if(level == 0)
            {
                return true;
            }
            return false;
        }

        public static bool Email(string value, int level = 0)
        {
            if(level == 0)
            {
                return true;
            }
            return false;
        }

        public static bool Password(string value, int level = 0)
        {
            Regex r = new Regex("^[a-zA-Z0-9]*$");
            if (level == 0 && value.Count() == 8)
            {
                return true;
            }
            else if(level == 1 && value.Count() == 8 && r.IsMatch(value))
            {
                return true;
            }
            return false;
        }
    }
}
