﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Winforms_UI.Libraries;
using Winforms_UI.Views.Account;
using Winforms_UI.Views.Admin;
using Winforms_UI.Views.Shared;
using MetroFramework.Forms;

namespace Winforms_UI
{
    public partial class MainPage : MetroForm
    {
        public MainPage()
        {
            InitializeComponent();
            Session.Start();
        }

        private void MainPage_Load(object sender, EventArgs e)
        {

            MainPanel.Controls.Clear();
            MainPanel.Controls.Add(new Dashboard());

            AccountSecurity.Login(new Models.AccountModel());

            if(AccountSecurity.IsLoggedin())
            {

            }
            else
            {
                Login loginpage = new Login();
                DialogResult dialogResult = loginpage.ShowDialog();
                
                if(dialogResult == DialogResult.OK)
                {
                    MainPanel.Controls.Clear();
                    MainPanel.Controls.Add(new _LockScreen());
                }
                else if(dialogResult == DialogResult.Cancel)
                {

                }
            }
        }
    }
}
